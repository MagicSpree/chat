import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Client implements ConnectionListener {

    private static final Logger log = Logger.getLogger(Client.class);
    private static final String IP_ADDR = "127.0.0.1";
    private static final int PORT = 8000;

    private Connection connection;

    Client() {
        try {
            connection = new Connection(this, IP_ADDR, PORT);
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            String inputUserString;
            while (true) {
                inputUserString = br.readLine();
                if (!inputUserString.isEmpty()) {
                    if (inputUserString.equals("quit")) {
                        connection.disconnect();
                        break;
                    }
                    String nameClient = connection.getSocketName();
                    connection.sendString(nameClient + ": " + inputUserString);
                }
            }
        } catch (IOException e) {
            log.error(e);
//            e.printStackTrace();
        }
    }

    @Override
    public void onConnectionReady(Connection connection) {
        printMessage("Connection ready...");
    }

    @Override
    public void onReceiveString(Connection connection, String value) {
        printMessage(value);
    }

    @Override
    public void onDisconnect(Connection connection) {
        printMessage("Connection close");
    }

    @Override
    public void onException(Connection connection, Exception e) {
        printMessage("Connection exception: " + e);
    }

    private void printMessage(String msg) {
        System.out.println(msg);
    }
}
