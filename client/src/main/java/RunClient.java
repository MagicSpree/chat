import org.apache.log4j.BasicConfigurator;

public class RunClient {
    public static void main(String[] args) {
        BasicConfigurator.configure();
        new Client();
    }
}
