import java.io.*;
import java.net.Socket;
import java.nio.charset.StandardCharsets;

public class Connection {

    private final Socket socket;
    private Thread rxThread;
    private final ConnectionListener connectionListener;
    private final BufferedReader in;
    private final BufferedWriter out;

    public Connection(ConnectionListener connectionListener, String ipAddress, int port) throws IOException {
        this(connectionListener, new Socket(ipAddress, port));
    }

    public Connection(ConnectionListener connectionListener, Socket socket) throws IOException {
        this.connectionListener = connectionListener;
        this.socket = socket;
        in = new BufferedReader(new InputStreamReader(socket.getInputStream(), StandardCharsets.UTF_8));
        out = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream(), StandardCharsets.UTF_8));
        rxThread = new Thread(() -> {
            try {
                connectionListener.onConnectionReady(Connection.this);
                while (!rxThread.isInterrupted()) {
                    connectionListener.onReceiveString(Connection.this, in.readLine());
                }
            } catch (IOException e) {
                connectionListener.onException(Connection.this, e);
            } finally {
                connectionListener.onDisconnect(Connection.this);
            }
        });
        rxThread.start();
    }

    public synchronized void sendString(String value) {
        try {
            out.write(value + "\r\n");
            out.flush();
        } catch (IOException e) {
            connectionListener.onException(Connection.this, e);
            disconnect();
        }
    }

    public synchronized void disconnect() {
        rxThread.interrupt();
        try {
            socket.close();
        } catch (IOException e) {
            connectionListener.onException(Connection.this, e);
        }
    }

    public String getSocketName() {
        return socket != null ? socket.getInetAddress().toString() : "";
    }

    @Override
    public String toString() {
        return "Connection: " + socket.getInetAddress() + ": " + socket.getPort();
    }
}
