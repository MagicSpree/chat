import org.apache.log4j.Logger;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.ArrayList;

public class Server implements ConnectionListener {

    private static final Logger log = Logger.getLogger(Server.class);
    private final ArrayList<Connection> connections = new ArrayList<>();

    public Server() {
        log.info("Сервер запущен...");
        try (ServerSocket socket = new ServerSocket(8000)) {
            while (true) {
                try {
                    new Connection(this, socket.accept());
                } catch (IOException e) {
                    log.error("Connection exception: " + e);
                }
            }
        } catch (IOException e) {
            log.error(e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public void onConnectionReady(Connection connection) {
        connections.add(connection);
        log.info("Client connected: " + connection);
    }

    @Override
    public synchronized void onReceiveString(Connection connection, String value) {
//        if ( value != null && !value.isEmpty())
        sendToAllClient(value);
    }

    @Override
    public synchronized void onDisconnect(Connection connection) {
        connections.remove(connection);
        log.info("Client disconnected: " + connection);
    }

    @Override
    public synchronized void onException(Connection connection, Exception e) {
        log.error("Connection exception: " + e);
    }

    private void sendToAllClient(String value) {
        if (value != null && !value.isEmpty())
            log.info(value);
        for (Connection connection : connections) {
            connection.sendString(value);
        }
    }
}
