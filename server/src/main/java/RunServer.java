import org.apache.log4j.BasicConfigurator;

public class RunServer {
    public static void main(String[] args) {
        BasicConfigurator.configure();
        new Server();
    }
}
